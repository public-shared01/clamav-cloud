const request = require('request-promise');

exports.quarantineScannedFile = async (data, context) => {
  const file = data;

  let options = {
    method: 'POST',
    uri: process.env.QUARANTINE_ENDPOINT,
    body: {
      location: `gs://${file.bucket}/${file.name}`,
      bucket: file.bucket,
      name: file.name,
      originalBucket: process.env.ORIGINAL_BUCKET,
      clean: false,
    },
    json: true
  }

  try {
    await request(options);
  } catch(e) {
    console.error(`Error occurred while trying to update storage object ${file.name}`, e);
  }
}
