#!/usr/bin/env sh
# SPDX-License-Identifier: GPL-2.0-or-later
# vim: set ts=4 sw=4 et:
set -eux

CVD_UPD_URL=172.17.0.2  # IP address of the container running the CVD update server

# run command if it is not starting with a "-" and is an executable in PATH
if [ "${#}" -gt 0 ] && \
   [ "${1#-}" = "${1}" ] && \
   command -v "${1}" > "/dev/null" 2>&1; then
	# Ensure healthcheck always passes
	CLAMAV_NO_CLAMD="true" exec "${@}"
else
	if [ "${#}" -ge 1 ] && \
	   [ "${1#-}" != "${1}" ]; then
		# If an argument starts with "-" pass it to clamd specifically
		exec clamd "${@}"
	fi
  # Set root user
  sed -i 's/clamav$/root/g' /usr/lib/systemd/system/clamav-daemon.socket
  sed -i '/\[Service\]/a User=root' /usr/lib/systemd/system/clamav-daemon.service
  sed -i "s/^User .*/User root/" /etc/clamav/clamd.conf

	# else default to running clamav's servers
  # Set Clam config file values
  # see clamd.conf documentation:
  # https://manpages.debian.org/bookworm/clamav-daemon/clamd.conf.5.en.html
  cat << EOF >> /etc/clamav/clamd.conf
#######################
StreamMaxLength 521M
OnAccessPrevention yes
OnAccessExcludeUname root

EOF

  # Update Freshclam config values
  # see freshclam.conf documentation:
  # https://manpages.debian.org/bookworm/clamav-freshclam/freshclam.conf.5.en.html
  #sed -i "s/database.clamav.net$/http:\/\/${CVD_UPD_URL}:8000/" /etc/clamav/freshclam.conf

  cat << EOF >> /etc/clamav/freshclam.conf
PrivateMirror http://${CVD_UPD_URL}:8000

EOF
	# Ensure we have some virus data, otherwise clamd refuses to start
	if [ ! -f "/var/lib/clamav/main.cvd" ]; then
		echo "Updating initial database"
		freshclam --foreground --stdout
	fi

	if [ "${CLAMAV_NO_FRESHCLAMD:-false}" != "true" ]; then
		echo "Starting Freshclamd"
		freshclam \
		          --checks="${FRESHCLAM_CHECKS:-1}" \
		          --daemon \
		          --foreground \
		          --stdout \
		          --user="clamav" \
			  &
	fi

	if [ "${CLAMAV_NO_CLAMD:-false}" != "true" ]; then
		echo "Starting ClamAV"
		if [ -S "/var/run/clamav/clamd.ctl" ]; then
			unlink "/var/run/clamav/clamd.ctl"
		fi
		clamd --foreground &&
		while [ ! -S "/var/run/clamav/clamd.ctl" ]; do
			if [ "${_timeout:=0}" -gt "${CLAMD_STARTUP_TIMEOUT:=180}" ]; then
				echo
				echo "Failed to start clamd"
				exit 1
			fi
			printf "\r%s" "Socket for clamd not found yet, retrying (${_timeout}/${CLAMD_STARTUP_TIMEOUT}) ..."
			sleep 1
			_timeout="$((_timeout + 1))"
		done
		echo "socket found, clamd started."
	fi

	if [ "${CLAMAV_ON_ACCESS:-true}" != "falce" ]; then
		echo "Starting ClamAV ob access mode"
		clamonacc &
	fi

	# Wait forever (or until canceled)
	exec tail -f "/dev/null"
fi

exit 0
